import {DebugElement, Type, Directive} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import { Component, EventEmitter } from '@angular/core';

export interface ThisContext<UnderTestType, MockType = {[key: string]: any}> {
  fixture: ComponentFixture<UnderTestType>;
  component: UnderTestType;
  debugElement: DebugElement;
  element: HTMLElement;
  service: UnderTestType;
  pipe: UnderTestType;
  interceptor: UnderTestType;
  mock: MockType;
  buildComponentAndSetupFixture(componentType: Type<UnderTestType>);
}

export const SetUpTestContext = <T>(thisContext: ThisContext<T>) => {
  thisContext.mock = {};

  thisContext.buildComponentAndSetupFixture = (componentType: Type<T>) => {
    const builtComponent = BuildComponent(componentType);

    thisContext.fixture = builtComponent.fixture;
    thisContext.component = builtComponent.component;
    thisContext.debugElement = builtComponent.debugElement;
    thisContext.element = builtComponent.element;
  };
};

export const CleanUpTestContext = <T>(thisContext: ThisContext<T>) => {
  if (document.body.contains(thisContext.element)) {
    document.body.removeChild(thisContext.element);
  }
};

export const BuildComponent = <T>(componentType: Type<T>): {fixture: ComponentFixture<T>, component: T, debugElement: DebugElement, element: HTMLElement} => {
  let component: T;
  let fixture: ComponentFixture<T>;
  let debugElement: DebugElement;
  let element: HTMLElement;

  fixture = TestBed.createComponent(componentType);

  component = fixture.componentInstance;

  debugElement = fixture.debugElement;
  element = debugElement.nativeElement;

  return {
    fixture,
    component,
    debugElement,
    element
  };
};

/**
 * Examples:
 * MockComponent({ selector: 'some-component' });
 * MockComponent({ selector: 'some-component', inputs: ['some-input', 'some-other-input'] });
 *
 * See https://angular.io/docs/ts/latest/api/core/index/Component-decorator.html for a list
 * of supported properties.
 */

export function MockComponent(options: Component): Component {

  const metadata: Component = {
    selector: options.selector,
    template: options.template || `<div class="mockComponent ${options.selector}"></div>`,
    inputs: options.inputs,
    outputs: options.outputs || []
  };

  class Mock {}

  metadata.outputs.forEach(method => {
    Mock.prototype[method] = new EventEmitter<any>();
  });

  return Component(metadata)(Mock as any);
}

export function MockDirective(options: Component): Component {
  const metadata: Directive = {
    selector: options.selector,
    inputs: options.inputs,
    outputs: options.outputs || []
  };

  class Mock {}

  metadata.outputs.forEach(method => {
    Mock.prototype[method] = new EventEmitter<any>();
  });

  return Directive(metadata)(Mock as any);
}
