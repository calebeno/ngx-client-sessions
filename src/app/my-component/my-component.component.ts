import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-my-component',
    templateUrl: './my-component.component.html',
    styleUrls: ['./my-component.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyComponentComponent implements OnInit {

    @Input() myName: string;
    @Input() myFlag: boolean;

    changedName: string;
    showCool = false;

    ngOnInit() {
        this.changedName = `Adventurer ${this.myName}`;
    }

    doSomethingCool() {
        alert('Something Quite Cool');
    }

    toggleDoSomething() {
        this.showCool = !this.showCool;
    }

}
