import {TestBed} from '@angular/core/testing';

import {MyComponentComponent} from './my-component.component';
import {CleanUpTestContext, SetUpTestContext, ThisContext} from '../test-utils/test.utils';

interface SpecMock {
    meh: string;
}

describe('View Alerts Component', function(this: ThisContext<MyComponentComponent, SpecMock>) {

    beforeEach(() => {
        SetUpTestContext(this);
    });

    afterEach(() => {
        CleanUpTestContext(this);
    });

    // Set up test component
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            declarations: [
                MyComponentComponent
            ],
            providers: []
        });
    });

    beforeEach(() => {
        this.buildComponentAndSetupFixture(MyComponentComponent);
    });

    it('should', () => {
        expect(true).toBe(true);
    });
});
